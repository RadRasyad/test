package matematika.rumus;

public class Matematika {

    public double tambah(int a, int b) {
        return (a+b);
    }

    public double kurang(int a, int b) {
        return (a-b);
    }

    public double kali(int a, int b) {
        return (a*b);
    }

    public double bagi(double a, double b) {
        return (a/b);
    }
}
