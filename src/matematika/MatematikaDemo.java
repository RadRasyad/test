package matematika;

import matematika.rumus.*;

public class MatematikaDemo {

    public static void main(String []args) {

        Matematika mtk = new Matematika();

        System.out.println("Pertambahan: 20+20 = " + mtk.tambah(20,20));
        System.out.println("Pengurangan: 10+5 = " + mtk.kurang(10,5));
        System.out.println("Perkalian: 10x20 = " + mtk.kali(10,20));
        System.out.println("Pembagian: 20/2 = " + mtk.bagi(20,2));
    }

}
