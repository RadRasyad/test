package Buku;

import Buku.model.Buku;

public class BukuDemo {

    public static void main(String []args) {
        Buku buku;
        buku = new Buku("Pemrograman Berbasis Objek dengan Java", "Indrajani", "Elexmedia Komputindo", 2007);
        buku.cetakBuku();
        buku = new Buku("Dasar Pemrograman Java", "Abdul Kadir", "Andi Offset", 2004);
        buku.cetakBuku();
    }

}
